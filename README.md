EPIDEMIO ENGINE BACK-END PROJECT
=====

### Requirements
This .net core project requires .net core 3.1. 

### Available HTTP Routes
(prefix /api)

 /Simulations
 /Diseases
 /Behaviour


### Docker Runner Guide

For testing the simulation in a production environment, use Docker.

**Option 1:** Make use of the pre-configured run-configurations for Jetbrains Rider

**Option 2:** Run docker commands yourself;


#### Build Engine Docker image

To run this project within a testing and (pre-)production environment you need to use Docker.

```bash
docker build -t windesheim_epidemo_engine_backend_image .
```

#### Docker Production Engine Compose 

```bash
# setup volume path for mssql persistence
export VOLUME=.:/var/data/project
# rebuild images on execution
docker-compose up -d --force-recreate --build
```


### Only want to debug the back-end application?
If you do not want to wait on the docker rebuild process everytime you made a change, it is possible to run the back-end application independently of the rest of the docker compose parts (socketio and mssql). 

**Be aware of a temporarly `Startup.cs` and `Services/SocketService.cs` change for using the available docker network socket.io and mssql server**


1. Start the docker configuration via the docker-compose commando's or the Rider Runner Configuration
2.1 Stop the Back-end Container in Rider > Services > Docker > {compose name} > Right click on container > Stop Container 
2.2 Stop the Back-end Container via commando:
```bash
# get docker container id of back-end runner
docker ps 
docker stop {insert_docker_back_end_container_id}
``` 
3. Start the Back-end application via the regular VS / Rider Debugger/Runner (it is possible to debug in Docker containers though)

## Grafana

Access grafana locally on [localhost:3000](http://localhost:3000) . Authorize with the following credentials to change dashboards e.d.:
user:admin
password:admin123