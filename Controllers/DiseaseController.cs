using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Epidemo_Engine_Back_end.Context;
using Epidemo_Engine_Back_end.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocketIOClient;

namespace Epidemo_Engine_Back_end.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DiseaseController : ControllerBase
    {
        private readonly EngineBackendContext _context;

        public DiseaseController(EngineBackendContext context)
        {
            _context = context;
        }

        // GET: api/Diseases
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Disease>>> GetDisease()
        {
            return await _context.Disease.ToListAsync();
        }

        // GET: api/Diseases/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Disease>> GetDisease(int id)
        {
            var disease = await _context.Disease.FindAsync(id);

            if (disease == null)
            {
                return NotFound();
            }

            return disease;
        }

        // PUT: api/Diseases/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDisease(int id, Disease disease)
        {
            if (id != disease.Id)
            {
                return BadRequest();
            }

            _context.Entry(disease).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DiseaseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        public async void EmitEvent(string eventName, object payload)
        {
            var socket = new SocketIO("http://socketio_srv:3010");
            // var socket = new SocketIO("http://localhost:3000");
            await socket.ConnectAsync();
            socket.OnConnected += async () => { await socket.EmitAsync(eventName, payload); };
        }


        // POST: api/Diseases
        [HttpPost]
        public async Task<ActionResult<Disease>> PostSimulation(Disease disease)
        {
            _context.Disease.Add(disease);
            await _context.SaveChangesAsync();

            EmitEvent("public_event", new {disease = disease});

            return CreatedAtAction("GetDisease", new {id = disease.Id}, disease);
        }

        // DELETE: api/Diseases/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Disease>> DeleteDisease(int id)
        {
            var disease = await _context.Disease.FindAsync(id);
            if (disease == null)
            {
                return NotFound();
            }

            _context.Disease.Remove(disease);
            await _context.SaveChangesAsync();

            return disease;
        }

        private bool DiseaseExists(int id)
        {
            return _context.Disease.Any(e => e.Id == id);
        }
    }
}