using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Epidemo_Engine_Back_end.Context;
using Epidemo_Engine_Back_end.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocketIOClient;

namespace Epidemo_Engine_Back_end.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SimulationsController : ControllerBase
    {
        private readonly EngineBackendContext _context;

        public SimulationsController(EngineBackendContext context)
        {
            _context = context;
        }

        // GET: api/Simulations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Simulation>>> GetSimulation()
        {
            return await _context.Simulation.ToListAsync();
        }

        // GET: api/Simulations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Simulation>> GetSimulation(int id)
        {
            var simulation = await _context.Simulation.FindAsync(id);

            if (simulation == null)
            {
                return NotFound();
            }

            return simulation;
        }

        // PUT: api/Simulations/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSimulation(int id, Simulation simulation)
        {
            if (id != simulation.Id)
            {
                return BadRequest();
            }

            _context.Entry(simulation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SimulationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Simulations
        [HttpPost]
        public async Task<ActionResult<Simulation>> PostSimulation(Simulation simulation)
        {
            _context.Simulation.Add(simulation);
            await _context.SaveChangesAsync();

            SocketHandler.EmitEvent("public_event", payload: new {@event = "simulation", data = simulation});

            return CreatedAtAction("GetSimulation", new {id = simulation.Id}, simulation);
        }

        // DELETE: api/Simulations/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Simulation>> DeleteSimulation(int id)
        {
            var simulation = await _context.Simulation.FindAsync(id);
            if (simulation == null)
            {
                return NotFound();
            }

            _context.Simulation.Remove(simulation);
            await _context.SaveChangesAsync();

            return simulation;
        }

        private bool SimulationExists(int id)
        {
            return _context.Simulation.Any(e => e.Id == id);
        }
    }
}