using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Epidemo_Engine_Back_end.Context;
using Epidemo_Engine_Back_end.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocketIOClient;

namespace Epidemo_Engine_Back_end.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BehaviorController : ControllerBase
    {
        private readonly EngineBackendContext _context;

        public BehaviorController(EngineBackendContext context)
        {
            _context = context;
        }

        // GET: api/behaviors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Behavior>>> GetBehavior()
        {
            return await _context.Behavior.ToListAsync();
        }

        // GET: api/behaviors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Behavior>> GetBehavior(int id)
        {
            var behavior = await _context.Behavior.FindAsync(id);

            if (behavior == null)
            {
                return NotFound();
            }

            return behavior;
        }

        // PUT: api/behaviors/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBehavior(int id, Behavior behavior)
        {
            if (id != behavior.Id)
            {
                return BadRequest();
            }

            _context.Entry(behavior).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BehaviorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        public async void EmitEvent(string eventName, object payload)
        {
            var socket = new SocketIO("http://socketio_srv:3010");
            // var socket = new SocketIO("http://localhost:3000");
            await socket.ConnectAsync();
            socket.OnConnected += async () => { await socket.EmitAsync(eventName, payload); };
        }


        // POST: api/behaviors
        [HttpPost]
        public async Task<ActionResult<Behavior>> PostSimulation(Behavior behavior)
        {
            _context.Behavior.Add(behavior);
            await _context.SaveChangesAsync();

            EmitEvent("public_event", new {behavior = behavior});

            return CreatedAtAction("Getbehavior", new {id = behavior.Id}, behavior);
        }

        // DELETE: api/behaviors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Behavior>> DeleteBehavior(int id)
        {
            var behavior = await _context.Behavior.FindAsync(id);
            if (behavior == null)
            {
                return NotFound();
            }

            _context.Behavior.Remove(behavior);
            await _context.SaveChangesAsync();

            return behavior;
        }

        private bool BehaviorExists(int id)
        {
            return _context.Behavior.Any(e => e.Id == id);
        }
    }
}