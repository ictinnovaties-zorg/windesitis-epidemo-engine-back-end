using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Epidemo_Engine_Back_end.Controllers
{
    [ApiController]
    [Route("/")]
    public class IndexController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return Json(new Dictionary<string, object>()
                {{"ok", "true"}, {"app", "Epidemo Engine Back-end Server"}});
        }
    }
}