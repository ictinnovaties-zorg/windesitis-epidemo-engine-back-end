using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Epidemo_Engine_Back_end.Models;
using SocketIOClient;
using EventHandler = SocketIOClient.EventHandler;

namespace Epidemo_Engine_Back_end.Controllers
{
    public static class SocketHandler
    {
        private static SocketIO socket;
        private static Guid backendId = Guid.NewGuid();

        static SocketHandler()
        {
        }

        public static async Task Connect(Action connectedCallback)
        {
            // socket = new SocketIO("http://localhost:3010")
            socket = new SocketIO("http://socketio_srv:3010")
            {
                Parameters = new Dictionary<string, string>()
                {
                    {
                        "backendId", backendId.ToString()
                    }
                },
                ConnectTimeout = TimeSpan.FromMilliseconds(2000)
            };

            await socket.ConnectAsync();
            socket.OnConnected += connectedCallback;
            Console.WriteLine($"Back-end connected to the SocketIO server with Id {backendId.ToString()}");
        }

        public static async void EmitEvent(string eventName, object payload)
        {
            Console.WriteLine($"Emitted {eventName} with payload {payload}");
            await socket.EmitAsync(eventName, payload);
        }

        public static void OnEvent(string eventName, EventHandler action)
        {
            Console.WriteLine($"listening for event {eventName}");
            socket.On(eventName, action);
        }
    }
}