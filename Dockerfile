FROM mcr.microsoft.com/dotnet/core/aspnet AS base
WORKDIR /app
EXPOSE 80
 
FROM mcr.microsoft.com/dotnet/core/sdk AS build
WORKDIR /src
COPY ["Epidemo Engine Back-end.csproj", ""]
RUN dotnet restore "Epidemo Engine Back-end.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "Epidemo Engine Back-end.csproj" -c Release -o /app
 
FROM build AS publish
RUN dotnet publish "Epidemo Engine Back-end.csproj" -c Release -o /app
 
FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Epidemo Engine Back-end.dll"]

# Available commands

### docker build -t windesheim_epidemo_engine_image -f Dockerfile . 

### after building use direct with run or make use of a container

# docker run windesheim_epidemo_engine_image

# docker create --name windesheim_epidemo_engine_image windesheim_epidemo_engine_image
