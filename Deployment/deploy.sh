#!/usr/bin/env bash

export BITBUCKET_BRANCH=$1
export VOLUME=/home/epidemio/backend/database:/var/opt/mssql

echo "Deploying back-end branch $BITBUCKET_BRANCH via user $USER"

mkdir -p /home/epidemio/backend/$BITBUCKET_BRANCH/
cd /home/epidemio/backend/$BITBUCKET_BRANCH/

if cd windesitis-epidemo-engine-back-end; then git checkout $BITBUCKET_BRANCH && git pull; else git clone --single-branch --branch $BITBUCKET_BRANCH git@bitbucket.org:ictinnovaties-zorg/windesitis-epidemo-engine-back-end.git && cd windesitis-epidemo-engine-back-end; fi

#docker-compose down && 
docker-compose up -d --force-recreate --build
