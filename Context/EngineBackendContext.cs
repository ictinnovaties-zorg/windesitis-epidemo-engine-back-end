using System;
using System.Collections.Generic;
using System.Linq;
using Epidemo_Engine_Back_end.Models;
using Microsoft.EntityFrameworkCore;

namespace Epidemo_Engine_Back_end.Context
{
    public class EngineBackendContext : DbContext
    {
        public EngineBackendContext(DbContextOptions<EngineBackendContext> options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }
        public DbSet<Models.Simulation> Simulation { get; set; }
        public DbSet<Models.Disease> Disease { get; set; }
        public DbSet<Models.Behavior> Behavior { get; set; }
        public DbSet<Models.Metric> Metric { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Disease>()
                .Property(d => d.Compartments)
                .HasConversion(
                    v => string.Join(',', v),
                    v => ToIntList(v));
            
            modelBuilder.Entity<Disease>().HasData(new Disease
            {
                Id = 1,
                Name = "Influenza",
                IncubationPeriod = 4, 
                InfectionChance = 0.20, 
                FatalityChance = 0,
                RecoveryChance = 0.25, 
                Compartments = new List<int> {0,1,2,3,4}
            },
                new Disease
            {
                Id = 2,
                Name = "Ebola",
                IncubationPeriod = 5, 
                InfectionChance = 0.5,
                FatalityChance = 0.05,
                RecoveryChance = 0.10, 
                Compartments = new List<int> {0,1,2,3}
            });

            modelBuilder.Entity<Behavior>().HasData(
                new Behavior
                {
                    Id = 1,
                    ContactAvoidanceOnSymptoms = 0.5,
                    VaccinationOnSymptoms = 0.1,
                    SeeDoctorOnSymptoms = 0.1,
                    DiagnosisSensitivity = 0.1,
                    UseDetectorOnSymptoms = 0.1
                }, new Behavior
                {
                    Id = 2,
                    ContactAvoidanceOnSymptoms = 0.2,
                    VaccinationOnSymptoms = 0.5,
                    SeeDoctorOnSymptoms = 0.4,
                    DiagnosisSensitivity = 0.6,
                    UseDetectorOnSymptoms = 0.7
                });
            
            base.OnModelCreating(modelBuilder);
        }

        private List<int> ToIntList(String source)
        {
            List<int> output = new List<int>();
            foreach (var s in source.Split(','))
            {
                output.Add(Int32.Parse(s));
            }

            return output;
        }
    }
}