using System;
using System.Threading;
using System.Threading.Tasks;
using Epidemo_Engine_Back_end.Context;
using Epidemo_Engine_Back_end.Controllers;
using Epidemo_Engine_Back_end.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace Epidemo_Engine_Back_end.Services
{
    public class SocketService : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;

        public SocketService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        private EngineBackendContext _context;
        private IServiceScope scope;

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await Task.Delay(15000);
            await SocketHandler.Connect(() => { });

            try
            {
                // Get the DbContext instance

                scope = _serviceProvider.CreateScope();
                _context = scope.ServiceProvider.GetRequiredService<EngineBackendContext>();
                _context.Database.EnsureCreated();

                SocketHandler.OnEvent("metric_update", (payload) =>
                {
                    try
                    {
                        string payloadString = payload.Text.ToString();
                        Metric metric = JsonConvert.DeserializeObject<Metric>(payloadString);
                        if (metric.Tick < 2)
                        {
                            // remove earlier metrics for the same simulation
                            _context.Database.ExecuteSqlRaw(
                                $"DELETE FROM Metric WHERE SimulationId={metric.SimulationId}");
                        }
                        try
                        {
                            AddMetricToDatabase(metric);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error with metrics Database Update");
                            Console.WriteLine(e);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error while deserializing socket payload for metrics update");
                        Console.WriteLine(e);
                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception with listening to socket or initalizing DB :: ");
                Console.WriteLine(e);
            }
        }

        private async Task AddMetricToDatabase(Metric metric)
        {
            Console.WriteLine($"Adding Metric for SimulationId {metric.SimulationId} to database.");
            _context.Metric.Add(metric);
            _context.SaveChanges();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}