using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Epidemo_Engine_Back_end.Models
{
    public class Disease
    {
        [Key] public int Id { get; set; }

        public string Name { get; set; }
        public List<int> Compartments { get; set; } //Type of states disease can apply to an individual.
        public int IncubationPeriod { get; set; }
        public double FatalityChance { get; set; } // dividing the number of deaths from a specified disease over a defined period of time by the number of individuals diagnosed with the disease during that time; 
        public double InfectionChance { get; set; }
        public double RecoveryChance { get; set; }
        public int RecoveryPeriod { get; set; }
        public bool ImmuneAfterRecovery { get; set; }

        public Disease()
        {
            
        }
        
        public Disease(
            int id,
            string name,
            int incubationPeriod,
            double infectionChance,
            double recoveryChance,
            List<int> compartments,
            int recoveryPeriod = 7,
            bool immuneAfterRecovery = false,
            double fatalityChance = 0)
        {
            Id = id;
            Name = name;
            Compartments = compartments;
            IncubationPeriod = incubationPeriod;
            FatalityChance = fatalityChance;
            InfectionChance = infectionChance;
            RecoveryChance = recoveryChance;
            ImmuneAfterRecovery = immuneAfterRecovery;
            RecoveryPeriod = recoveryPeriod;
        }


        // public int N;
        // public SimulationParameters Parameters;
    }
    
//    public enum PersonState
//    {
//        // Covers most diseases
//        Susceptible = 0, // Healthy, susceptible to infection
//        Exposed = 1, // Incubation period, person is infected, asymptomatic and infectious 
//        Infectious = 2, // Infected, has symptoms, can infect others
//        Recovered = 3, // Healthy again, not susceptible period
//        Immune = 4, // Immune by antibodies or vaccine.
//        Dead = 5,
//    }
}