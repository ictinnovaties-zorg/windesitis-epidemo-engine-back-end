using System;
using System.ComponentModel.DataAnnotations;

namespace Epidemo_Engine_Back_end.Models
{
    public class Simulation
    {
        [Key] public int Id { get; set; }

        public string Name { get; set; }
        public int N { get; set; }
        public int T { get; set; }
        public int SimulationState { get; set; }
        public int DiseaseId { get; set; }
        public double DetectorPrevalence { get; set; }
        public int BehaviorId { get; set; }
        public double TickMultiplier { get; set; }
        public DateTime StartTime { get; set; }

        // public int N;
        // public SimulationParameters Parameters;
    }
}