using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Epidemo_Engine_Back_end.Models
{
    public class Metric
    {
        [Key] public int Id { get; set; }
        
        /// <summary>
        /// The of the simulation.
        /// </summary>
        public int SimulationId { get; set; }
        public int PopulationId { get; set; }

        /// <summary>
        /// The actual, real-world time this snapshot was taken
        /// </summary>
        ///
        public DateTime TimeOfSnapshot { get; set; }

        /// <summary>
        /// The simulation RealTime time 
        /// </summary>
        public DateTime RealTime { get; set; }
        
        /// <summary>
        /// The TickMultiplier of the simulation
        /// </summary>
        public double TickMultiplier { get; set; }

        public DateTime StartTime { get; set; }

        /// <summary>
        /// The size of the population
        /// </summary>
        public int PopulationSize { get; set; }


        public int TotalInfected { get; set; }
        public int TotalInfectious { get; set; }
        public int TotalRecovered { get; set; }
        public int TotalExposed { get; set; }
        public int TotalSusceptible { get; set; }
        public int TotalImmune { get; set; }
        public int TotalDead { get; set; }
        // End state compartment metrics
        
        // Start suspected state compartment metrics
        public int TotalSuspectedSafe { get; set; }
        public int TotalSuspectedAtRisk { get; set; }
        public int TotalSuspectedPossibleCarriers { get; set; }
        public int TotalSuspectedConfirmedCarriers { get; set; }
        // End state compartment metrics
        public double DetectorPrevalence { get; set; }
        
        public int PersonsWithDetector { get; set; }
        public double AveragePf { get; set; }
        public double AveragePb { get; set; }
        public double AveragePartnerships { get; set; }
        public double MaxPartnerships { get; set; }
        public string DiseaseName { get; set; }
        public int BehaviourId { get; set; }

        /// <summary>
        /// The duration of the last tick
        /// </summary>
        public double LastTickDuration { get; set; }
        public int Tick { get; set; }

        [NotMapped] private Simulation Simulation;
    }
}