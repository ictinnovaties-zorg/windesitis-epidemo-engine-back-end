#!/usr/bin/env bash

export BITBUCKET_BRANCH=$1

echo "Deploying Metrics server via the production instance on branch $BITBUCKET_BRANCH"

mkdir -p /home/epidemio/$BITBUCKET_BRANCH/
cd /home/epidemio/$BITBUCKET_BRANCH/

if cd windesitis-epidemo-engine; then git checkout $BITBUCKET_BRANCH && git pull; else git clone --single-branch --branch $BITBUCKET_BRANCH git@bitbucket.org:ictinnovaties-zorg/windesitis-epidemo-engine.git && cd windesitis-epidemo-engine; fi
cd "Epidemo Engine/Metrics"

docker-compose down && docker-compose up -d
