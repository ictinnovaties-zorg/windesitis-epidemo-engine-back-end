using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Epidemo_Engine_Back_end.Context;
using Epidemo_Engine_Back_end.Controllers;
using Epidemo_Engine_Back_end.Models;
using Epidemo_Engine_Back_end.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Epidemo_Engine_Back_end
{
    public class Program
    {
        public static void Main(string[] args)
        {
           

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); }).ConfigureServices(
                    services => { services.AddHostedService<SocketService>(); });
    }
}