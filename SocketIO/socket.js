var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');
const request = require('request');

app.listen(3010);

function handler(req, res) {
  fs.readFile(__dirname + '/index.html',
      function(err, data) {
        if (err) {
          res.writeHead(500);
          return res.end('Error loading index.html');
        }

        res.writeHead(200);
        res.end(data);
      });
}

console.log('SocketIO Server Running on localhost:3010');

let simulation_sockets = [];
let backendId = null;

io.on('connection', function(socket) {

  console.log('Peer connected');
  let connectionIsEngine = false;

  var simulationId =
      socket.handshake.query.simulationId;

  if (socket.handshake.query.is_engine) connectionIsEngine = true;

  if (socket.handshake.query.backendId) {
    backendId = socket.handshake.query.backendId;
    socket.join(backendId);
    console.log('Backend connected with Id ' + backendId);
  }
  if (simulationId) {

    socket.join(simulationId);
    console.log('Peer joined simulation_socket ' + simulationId);
    if (!simulation_sockets.includes(simulationId)) simulation_sockets.push(
        simulationId);
  }

  socket.on('public_event', function(data) {
    // console.log(data);
    console.log({[data.event]: data.data});
    io.emit(data.event, data.data);
  });

  if (simulationId) {
    // received metricupdate from sim
    socket.on('metric_update', function(data) {
      console.log(
          'metricupdate from ' + simulationId + ' emit to B ' + backendId);
      io.to(backendId).emit('metric_update', data);
    });
  }

  socket.on('simulation_event', function(data) {
    // console.log('simulation_control: ' + data);
    console.log({[data.event]: data.data});
    io.to(simulationId).emit(data.event, data.data);
    // io.to(simulation).emit('simulation_control', data);
  });

  socket.on('disconnect', () => {
    if (simulationId) {

      console.log(
          'Lost connection with peer with simulationId ' + simulationId);

      simulation_sockets.splice(simulation_sockets.indexOf(simulationId, -1));
      if (connectionIsEngine) {
        // run delete 
        // request.del('https://api.epidemio.run/api/Simulations/' + simulationId,
        request.del('http://epidemo_engine_backend_server/api/Simulations/' +
            simulationId,
            (err, res, body) => {
              if (err) { return console.log(err); }
              console.log(body);
              console.log('Cleaned simulation backend entry ' + simulationId);
              io.emit('simulation', {Id: simulationId});
            });
      }
    }
  });
});